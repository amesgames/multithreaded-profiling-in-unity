﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

using UnityEngine;
using UnityEngine.UI;

using Amesgames.Threading;

namespace Amesgames.Behaviours
{
	public class ProfiledWorkerThreadBehaviour : MonoBehaviour, IProfilerConsumer
	{
		public void Start()
		{
			threads.Add(new ProfiledWorkerThread(ShortWork, "short"));
			threads.Add(new ProfiledWorkerThread(LongWork, "long"));

			text = GameObject.FindObjectOfType<Text>();

			Profiler.AddConsumer(this);

			Profiler.SetThreadName("main");

			foreach(ProfiledWorkerThread thread in threads)
				thread.Wait();
		}

		public void Update()
		{
			// Clean up the previous frame of the main thread at the top of our
			// Update. This ensures that we capture all of the main thread
			// scripts as run time. Make sure this unity script's execution
			// order is earlier than the others.
			if(profilerInitialized)
			{
				Profiler.WaitFrameThread();

				foreach(ProfiledWorkerThread thread in threads)
					thread.Done();

				Profiler.EndFrameThread();

				foreach(ProfiledWorkerThread thread in threads)
					thread.Finish();

				foreach(ProfiledWorkerThread thread in threads)
					thread.Wait();
			}

			profilerInitialized = true;

			Profiler.DemarcateFrame();

			foreach(ProfiledWorkerThread thread in threads)
				thread.Run();

			Profiler.BeginFrameThread();

			Profiler.BeginSample("update");

			lock(stepCountByThreadId)
			{
				text.text += "\n";
				foreach(KeyValuePair<int, int> kv in stepCountByThreadId)
				{
					Profiler.BeginSample("text");

					text.text += "thread " + kv.Key + " count " + kv.Value + "\n";

					Profiler.EndSample();
				}
			}

			CountAndSleep(25);

			Profiler.EndSample();
		}

		public void ReceiveFrame(ProfilerFrame frame)
		{
			text.text = "";
			ProfilerThread[] threads = frame.threads.ToArray();
			Array.Sort(threads, (x, y) => x.name.CompareTo(y.name));
			foreach(ProfilerThread thread in threads)
			{
				text.text += thread.name + ": run: " +
					thread.runMs.ToString() + " ms wait: " +
					thread.waitMs.ToString() + " ms\n";

				ReceiveSamples(thread, 2);
			}
		}

		readonly Dictionary<int, int> stepCountByThreadId =
			new Dictionary<int, int>();
		Text text;
		readonly List<ProfiledWorkerThread> threads =
			new List<ProfiledWorkerThread>();
		bool profilerInitialized = false;

		void ReceiveSamples(ProfilerSamples samples, int indent)
		{
			foreach(ProfilerSample sample in samples.samples)
				ReceiveSample(sample, indent + 2);
		}

		void ReceiveSample(ProfilerSample sample, int indent)
		{
			for(int i = 0; i < indent; i++)
				text.text += " ";

			text.text += sample.name + ": " + sample.runMs + " ms\n";

			ReceiveSamples(sample, indent);
		}

		void ShortWork()
		{
			CountAndSleep(10);
		}

		void LongWork()
		{
			CountAndSleep(50);
		}

		void CountAndSleep(int milliseconds)
		{
			Profiler.BeginSample("count and sleep");

			Profiler.BeginSample("count");

			lock(stepCountByThreadId)
			{
				int threadId = Thread.CurrentThread.ManagedThreadId;
				int count;
				if(stepCountByThreadId.TryGetValue(threadId, out count))
					stepCountByThreadId[threadId] = count + 1;
				else
					stepCountByThreadId.Add(threadId, 1);
			}

			Profiler.EndSample();

			Profiler.BeginSample("sleep");

			Thread.Sleep(milliseconds);

			Profiler.EndSample();

			Profiler.EndSample();
		}
	}
}
