﻿using System.Threading;

using UnityEngine;

namespace Amesgames.Behaviours
{
	public class UnityProfilerExceptionBehaviour : MonoBehaviour
	{
		void Start()
		{
			new Thread(new ThreadStart(Work)).Start();
		}

		void Work()
		{
			UnityEngine.Profiler.BeginSample("test");
			UnityEngine.Profiler.EndSample();
		}
	}
}
