﻿using System;
using System.Collections.Generic;
using System.Threading;

using UnityEngine;
using UnityEngine.UI;

using Amesgames.Threading;

namespace Amesgames.Behaviours
{
	public class FramedWorkerThreadBehaviour : MonoBehaviour
	{
		public void Start()
		{
			threads.Add(new FramedWorkerThread(ShortWork));
			threads.Add(new FramedWorkerThread(LongWork));

			text = GameObject.FindObjectOfType<Text>();
		}

		void CountAndSleep(int milliseconds)
		{
			lock(stepCountByThreadId)
			{
				int threadId = Thread.CurrentThread.ManagedThreadId;
				int count;
				if(stepCountByThreadId.TryGetValue(threadId, out count))
					stepCountByThreadId[threadId] = count + 1;
				else
					stepCountByThreadId.Add(threadId, 1);
			}
			Thread.Sleep(milliseconds);
		}

		public void Update()
		{
			foreach(FramedWorkerThread thread in threads)
				thread.Wait();

			foreach(FramedWorkerThread thread in threads)
				thread.Run();

			lock(stepCountByThreadId)
			{
				text.text = "";
				foreach(KeyValuePair<int, int> kv in stepCountByThreadId)
					text.text += "thread " + kv.Key + " count " + kv.Value + "\n";
			}

			CountAndSleep(25);
		}

		readonly Dictionary<int, int> stepCountByThreadId =
			new Dictionary<int, int>();
		Text text;
		readonly List<FramedWorkerThread> threads = new List<FramedWorkerThread>();

		void ShortWork()
		{
			CountAndSleep(10);
		}

		void LongWork()
		{
			CountAndSleep(50);
		}
	}
}
