﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace Amesgames
{
	public interface IProfilerConsumer
	{
		void ReceiveFrame(ProfilerFrame frame);
	}

	public class ProfilerSamples
	{
		public readonly List<ProfilerSample> samples = new List<ProfilerSample>();
	}

	public class ProfilerSample : ProfilerSamples
	{
		public string name;
		public long beginTicks;
		public long endTicks;

		public ProfilerSample(string name, long ticks)
		{
			this.name = name;
			beginTicks = ticks;
		}

		public float runMs {
			get {
				if(endTicks > beginTicks)
					return (endTicks - beginTicks) / (float)Stopwatch.Frequency;
				return 0.0f;
			}
		}
	}

	public class ProfilerThread : ProfilerSamples
	{
		public string name;
		public long startTicks;
		public long waitTicks;
		public long endTicks;

		public ProfilerThread(string name, long ticks)
		{
			this.name = name;
			this.startTicks = ticks;
		}

		public float runMs {
			get {
				if(waitTicks > startTicks)
					return (waitTicks - startTicks) / (float)Stopwatch.Frequency;
				return 0.0f;
			}
		}

		public float waitMs {
			get {
				if(endTicks > waitTicks)
					return (endTicks - waitTicks) / (float)Stopwatch.Frequency;
				return 0.0f;
			}
		}

		public float totalMs {
			get {
				if(endTicks > startTicks)
					return (endTicks - startTicks) / (float)Stopwatch.Frequency;
				return 0.0f;
			}
		}
	}

	public class ProfilerFrame
	{
		public readonly List<ProfilerThread> threads = new List<ProfilerThread>();
	}

	public class Profiler
	{
		public static void AddConsumer(IProfilerConsumer consumer)
		{
			if(!consumers.Contains(consumer))
				consumers.Add(consumer);
		}

		public static void RemoveConsumer(IProfilerConsumer consumer)
		{
			if(consumers.Contains(consumer))
				consumers.Remove(consumer);
		}

		public static void SetThreadName(string name)
		{
			if(name != null && name.Length > 0)
				Name = name;
		}

		public static void DemarcateFrame()
		{
			if(frame != null)
			{
				lock(samplesByThreadId)
				{
					foreach(ProfilerThread thread in samplesByThreadId.Values)
						frame.threads.Add(thread);

					samplesByThreadId.Clear();
				}

				foreach(IProfilerConsumer consumer in consumers)
					consumer.ReceiveFrame(frame);
			}

			frame = new ProfilerFrame();
		}

		public static void BeginFrameThread()
		{
			ProfilerThread thread;
			lock(samplesByThreadId)
			{
				thread = new ProfilerThread(Name, Stopwatch.ElapsedTicks);
				samplesByThreadId.Add(Thread.CurrentThread.ManagedThreadId, thread);
			}
			Samples.Push(thread);
		}

		public static void BeginSample(string name)
		{
			Samples.Push(new ProfilerSample(name, Stopwatch.ElapsedTicks));
		}

		public static void EndSample()
		{
			ProfilerSample profilerSample = (ProfilerSample)Samples.Pop();
			profilerSample.endTicks = Stopwatch.ElapsedTicks;
			Samples.Peek().samples.Add(profilerSample);
		}

		public static void WaitFrameThread()
		{
			lock(samplesByThreadId)
			{
				int threadId = Thread.CurrentThread.ManagedThreadId;
				ProfilerThread thread = samplesByThreadId[threadId];
				thread.waitTicks = Stopwatch.ElapsedTicks;
			}
		}

		public static void EndFrameThread()
		{
			lock(samplesByThreadId)
			{
				int threadId = Thread.CurrentThread.ManagedThreadId;
				ProfilerThread thread = samplesByThreadId[threadId];
				thread.endTicks = Stopwatch.ElapsedTicks;
			}
			Samples.Pop();
		}

		static ProfilerFrame frame;
		static readonly Dictionary<int, ProfilerThread> samplesByThreadId =
			new Dictionary<int, ProfilerThread>();

		[ThreadStatic]
		static Stack<ProfilerSamples> _samples;

		static Stack<ProfilerSamples> Samples {
			get {
				if(_samples == null)
					_samples = new Stack<ProfilerSamples>();
				return _samples;
			}
		}

		static Stopwatch Stopwatch {
			get {
				if(_stopwatch == null)
				{
					_stopwatch = new Stopwatch();
					_stopwatch.Start();
				}
				return _stopwatch;
			}
		}

		[ThreadStatic]
		static Stopwatch _stopwatch;

		static string Name {
			get {
				if(_name == null)
					_name = string.Format("thread {0}",
						Thread.CurrentThread.ManagedThreadId);
				return _name;
			}
			set {
				_name = value;
			}
		}

		[ThreadStatic]
		static string _name;

		static readonly List<IProfilerConsumer> consumers = new List<IProfilerConsumer>();
	}
}
