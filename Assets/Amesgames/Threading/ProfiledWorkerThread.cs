﻿using System;
using System.Threading;

namespace Amesgames.Threading
{
	public class ProfiledWorkerThread
	{
		public ProfiledWorkerThread(Action step, string name = "")
		{
			this.name = name;
			this.step = step;

			thread = new Thread(new ThreadStart(Work));
			thread.Start();
		}

		public void Wait()
		{
			ready.WaitOne();
		}

		public void Run()
		{
			go.Set();
		}

		public void Done()
		{
			done.WaitOne();
		}

		public void Finish()
		{
			wait.Set();
		}

		string name;
		Action step;
		Thread thread;

		// Signals main thread that we are ready to run the next step
		AutoResetEvent ready = new AutoResetEvent(/*initialState=*/false);

		// Used by main thread to signal us to run next step
		AutoResetEvent go = new AutoResetEvent(/*initialState=*/false);

		// Signals main thread that we are done with last step
		AutoResetEvent done = new AutoResetEvent(/*initialState=*/false);

		// Used by main thread to signal us that we can stop waiting for rest
		// of threads.
		AutoResetEvent wait = new AutoResetEvent(/*initialState=*/false);

		void Work()
		{
			Profiler.SetThreadName(name);

			ready.Set();

			go.WaitOne();

			while(true)
			{
				Profiler.BeginFrameThread();

				try
				{
					step();
				}
				catch(Exception e)
				{
					UnityEngine.Debug.LogException(e);
				}

				Profiler.WaitFrameThread();

				done.Set();

				wait.WaitOne();

				Profiler.EndFrameThread();

				ready.Set();

				go.WaitOne();
			}
		}
	}
}
