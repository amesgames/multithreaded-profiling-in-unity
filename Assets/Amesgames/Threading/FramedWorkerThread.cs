﻿using System;
using System.Threading;

namespace Amesgames.Threading
{
	public class FramedWorkerThread
	{
		public FramedWorkerThread(Action step)
		{
			this.step = step;

			thread = new Thread(new ThreadStart(Work));
			thread.Start();
		}

		public void Wait()
		{
			ready.WaitOne();
		}

		public void Run()
		{
			go.Set();
		}

		Action step;
		Thread thread;
		AutoResetEvent ready = new AutoResetEvent(/*initialState=*/false);
		AutoResetEvent go = new AutoResetEvent(/*initialState=*/false);

		void Work()
		{
			while(true)
			{
				ready.Set();
				go.WaitOne();

				step();
			}
		}
	}
}
