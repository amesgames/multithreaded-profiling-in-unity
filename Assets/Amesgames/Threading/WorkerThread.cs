﻿using System;
using System.Threading;

namespace Amesgames.Threading
{
	public class WorkerThread
	{
		public WorkerThread(Action step)
		{
			this.step = step;

			thread = new Thread(new ThreadStart(Work));
			thread.Start();
		}

		Action step;
		Thread thread;

		void Work()
		{
			while(true)
				step();
		}
	}
}
